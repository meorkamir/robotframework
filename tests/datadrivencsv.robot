*** Settings ***
Resource  ../resources/common.robot
Resource  ../resources/OrangeHRMApp.robot

Resource  ../data/inputdata.robot
Resource  ../resources/DataManager.robot

Test Setup  Begin Browser
Test Teardown  End Browser

*** Keywords ***

*** Test Cases ***
Should See Correct Error message when login with incorrect credential
    ${InvalidLoginScenario} =  DataManager.Get CSV Data  ${INVALID_CREDENTIAL_PATH_CSV}
    OrangeHRMApp.Login With Multiple Invalid Credentials  ${InvalidLoginScenario}