*** Settings ***
Resource  ../resources/common.robot
Resource  ../resources/pages/login.robot
Resource  ../resources/pages/dashboard.robot

Test Setup  Begin Browser
Test Teardown  End Browser

*** Variables ***
#${BROWSER} =  firefox
#${URL} =  https://opensource-demo.orangehrmlive.com

*** Test Cases ***
User Should Able To Login
    [Documentation]  User should able to access login functionality
    [Tags]  Smoke
    login.User Input Username  admin
    login.User Input Password  admin123
    login.Click Login
    login.User Successfully Login Into Dashboard

User Should Able To View Leave List
    [Documentation]  User should able to view leave list
    [Tags]  Smoke
    login.User Perform Login  admin  admin123
    dashboard.User Click Leave List Link



