*** Settings ***
Resource  ../resources/common.robot
Resource  ../resources/OrangeHRMApp.robot

Resource  ../data/inputdata.robot

Test Setup  Begin Browser
Test Teardown  End Browser

*** Variables ***

*** Keywords ***


*** Test Cases ***
Unregistered user should see corret error message at login page
    OrangeHRMApp.User Perform Login  ${INVALID_USER_CREDENTIAL}
    OrangeHRMApp.Verify Login Page Error Message  ${INVALID_USER_CREDENTIAL}

#Login with incorrect password should display correct error messgae

Login with blank email and password should shoe correct error message
    OrangeHRMApp.User Perform Login  ${BLANK_USER_CREDENTIAL}
    OrangeHRMApp.Verify Login Page Error Message  ${BLANK_USER_CREDENTIAL}