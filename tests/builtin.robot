*** Settings ***
Resource  ../resources/common.robot
Resource  ../resources/OrangeHRMApp.robot

Resource  ../data/inputdata.robot

Test Setup  Begin Browser
Test Teardown  End Browser

*** Variables ***

*** Keywords ***


*** Test Cases ***
Invalid login scenario should display correct messages
    [Template]  Test Multiple Login Scenario
    ${INVALID_USER_CREDENTIAL}
    ${BLANK_USER_CREDENTIAL}
