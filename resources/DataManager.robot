*** Settings ***
Documentation  Use to retrieve data from csv file
Library  ../CustomLib/Csv.py

*** Keywords ***
Get CSV Data
    [Arguments]  ${filepath}
    ${data} =  read csv file  ${filepath}
    [Return]   ${data}