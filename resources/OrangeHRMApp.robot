*** Settings ***
Resource  ../resources/pages/login.robot

*** Variables ***


*** Keywords ***
Login With Multiple Invalid Credentials
    [Documentation]  Created for data driven using CSV File
    [Arguments]  ${InvalidLoginScenarios}

    :FOR  ${LoginScenario}  IN  @{InvalidLoginScenarios}
    \    run keyword and continue on failure  User Perform Login  ${LoginScenario}
    \    run keyword and continue on failure  Verify Login Page Error Message  ${LoginScenario}

Test Multiple Login Scenario
    [Documentation]  Created for data driven using TEMPLATE(BuiltIn)
    [Arguments]  ${Credential}
    User Perform Login  ${Credential}
    Verify Login Page Error Message  ${Credential}

User Perform Login
    [Arguments]  ${Credential}
    Login.User Input Username  ${Credential}
    Login.User Input Password  ${Credential}
    Login.Click Login

Verify Login Page Error Message
    [Arguments]  ${Credential}
    Login.Verify Error Message  ${Credential[2]}
