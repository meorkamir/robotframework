*** Settings ***
Library  SeleniumLibrary
Library  BuiltInLibrary

*** Variables ***
${LOGIN_USERNAME_TXTBOX} =  name=txtUsername
${LOGIN_PASSWORD_TXTBOX} =  name=txtPassword
${LOGIN_BUTTON} =  name=Submit

*** Keywords ***
User Input Username
    [Arguments]  ${Credential}
    run keyword unless  '${Credential[0]}' == '#BLANK'  Input Text  ${LOGIN_USERNAME_TXTBOX}  ${Credential[0]}

User Input Password
    [Arguments]  ${Credential}
    run keyword unless  '${Credential[1]}' == '#BLANK'  Input Password  ${LOGIN_PASSWORD_TXTBOX}  ${Credential[1]}

Click Login
    Click Button  ${LOGIN_BUTTON}

User Successfully Login Into Dashboard
    Wait Until Page Contains Element  id=welcome

Verify Error Message
    [Arguments]  ${ExpectedErrorMessage}
    Element Text Should Be  id=spanMessage  ${ExpectedErrorMessage}
