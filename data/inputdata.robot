*** Variables ***
# configuration
${BROWSER} =  firefox
${ENV} =  dev
&{URL}  dev=https://opensource-demo.orangehrmlive.com  stage=https://opensource-demo.orangehrmlive.com
${INVALID_CREDENTIAL_PATH_CSV} =  data/user.csv


# input data
&{INVALID_USER_CREDENTIAL}  username=admin  password=admin  expectedmessage=Invalid credentials
&{VALID_USER_CREDENTIAL}  username=admin  password=admin123
&{BLANK_USER_CREDENTIAL}  username=#BLANK  password=#BLANK  expectedmessage=Username cannot be empty
